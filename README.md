# Dotfiles

Collection of dot files with my prefered configurations

## Installation

```bash
curl -fsSL https://gitlab.com/alexebaker/dotfiles/raw/master/install.sh | sh
```
